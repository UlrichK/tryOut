package example.tryOut.ui;

import java.util.*;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.markup.html.tabs.AbstractTab;
import org.apache.wicket.extensions.markup.html.tabs.ITab;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import de.agilecoders.wicket.core.markup.html.bootstrap.button.BootstrapAjaxButton;
import de.agilecoders.wicket.core.markup.html.bootstrap.button.Buttons;
import de.agilecoders.wicket.core.markup.html.bootstrap.dialog.Modal;
import de.agilecoders.wicket.core.markup.html.bootstrap.form.BootstrapForm;
import de.agilecoders.wicket.core.markup.html.bootstrap.tabs.ClientSideBootstrapTabbedPanel;
import de.agilecoders.wicket.extensions.markup.html.bootstrap.form.DateTextField;
import de.agilecoders.wicket.extensions.markup.html.bootstrap.form.validation.SimpleMessageValidation;
import example.tryOut.ows.Ows;
import example.tryOut.ows.OwsType;

@SuppressWarnings("ALL")
public abstract class OwsDetailsDialog extends Modal<Ows>
{
    private static final long serialVersionUID = -8110788978602397064L;

    private BootstrapAjaxButton createOwsBtn;
    private BootstrapForm<Ows> owsForm;

    public OwsDetailsDialog(String id,
            IModel<Ows> owsModel)
    {
        super(id, owsModel);
        setOutputMarkupPlaceholderTag(true);
        setDefaultModel(owsModel);

        owsForm = new BootstrapForm<>("owsForm");
        owsForm.setOutputMarkupId(true);
        add(owsForm);

        List<ITab> tabs = createTabs(owsModel);
        MyAjaxTabbedPanel tabbedPanel = new MyAjaxTabbedPanel("tabbedPanel", tabs);
        owsForm.add(tabbedPanel);
        tabbedPanel.setOutputMarkupId(true);

        SimpleMessageValidation validation = new SimpleMessageValidation();
        validation.getConfig().appendToParent(true);
        owsForm.add(validation);

        createOwsBtn = new BootstrapAjaxButton("submitOws",
                Model.of("Create"), Buttons.Type.Default)
        {
            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form)
            {
                super.onError(target, form);
                target.add(form, tabbedPanel);
                System.out.println("onError() at form ID " + form.getId() + " target.getPage() "
                        + target.getPage());
            }

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form)
            {
                super.onSubmit(target, form);
                System.out.println("click on create");
                createOws(target, owsModel);
            }
        };
        owsForm.add(createOwsBtn);

        BootstrapAjaxButton cancelBtn = new BootstrapAjaxButton("cancelOws",
                Model.of("Cancel"), Buttons.Type.Default)
        {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form)
            {
                super.onSubmit(target, form);
                cancel(target);
            }
        };
        cancelBtn.setDefaultFormProcessing(false);
        owsForm.add(cancelBtn);

        //FeedbackPanel feedbackOwsDialog = new FeedbackPanel("feedbackOwsDialog");
        //add(feedbackOwsDialog);
    }

    private List<ITab> createTabs(IModel<Ows> owsModel)
    {
        List<ITab> tabs = new ArrayList<>();

        tabs.add(new AbstractTab(Model.of("Data 1"))
        {
            private static final long serialVersionUID = -9107223557866453561L;

            @Override
            public WebMarkupContainer getPanel(String panelId)
            {
                return new OwsMainInfoContainer(panelId, owsModel);
            }
        });
        tabs.add(new AbstractTab(Model.of("Data 2"))
        {
            private static final long serialVersionUID = -7323530522820254738L;

            @Override
            public WebMarkupContainer getPanel(String panelId)
            {
                return new OwsSecondaryInfoContainer(panelId, owsModel);
            }
        });

        return tabs;
    }

    protected class OwsMainInfoContainer extends Panel
    {
        private static final long serialVersionUID = -2965824809083715016L;

        public OwsMainInfoContainer(
                String panelId, IModel<Ows> owsModel)
        {
            super(panelId, owsModel);
            add(new RequiredTextField<>("title"));
            add(new RequiredTextField<>("url"));

            List<OwsType> types = Arrays.asList(OwsType.values());

            DropDownChoice<OwsType> dropDownChoice = new DropDownChoice<>("category", types,
                    new ChoiceRenderer<OwsType>()
                    {
                        private static final long serialVersionUID = 8139757791037487164L;

                        @Override
                        public Object getDisplayValue(OwsType owsType)
                        {
                            return owsType.getName();
                        }
                    });

            add(dropDownChoice.setRequired(true));

            //add(new FeedbackPanel("feedbackMain"));
        }
    }

    protected class OwsSecondaryInfoContainer extends Panel
    {
        private static final long serialVersionUID = -7396160769731997541L;

        public OwsSecondaryInfoContainer(
                String panelId, IModel<Ows> owsModel)
        {
            super(panelId, owsModel);
            add(new DateTextField("firstPublished"));
            add(new TextField<>("provider").setRequired(true));
            add(new TextField("price").setRequired(true));
            //add(new FeedbackPanel("feedbackSecondary"));
        }
    }

    protected abstract void createOws(AjaxRequestTarget target
            , IModel<Ows> owsWithUtilDateIModel);

    protected abstract void cancel(AjaxRequestTarget target);

    // http://www.volkomenjuist.nl/blog/2009/12/01/ajaxtabbedpanel-store-state-when-switching-tabs/
    //class MyAjaxTabbedPanel extends AjaxBootstrapTabbedPanel<ITab>
    class MyAjaxTabbedPanel extends ClientSideBootstrapTabbedPanel<ITab>
    {
        private static final long serialVersionUID = 1513951445901529991L;

        public MyAjaxTabbedPanel(String id, List<ITab> tabs)
        {
            super(id, tabs);
        }
    }
}
