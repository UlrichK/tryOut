package example.tryOut.ui;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.*;

import de.agilecoders.wicket.core.markup.html.bootstrap.button.BootstrapAjaxButton;
import de.agilecoders.wicket.core.markup.html.bootstrap.button.Buttons;
import example.tryOut.ows.Ows;

public class MyPage extends WebPage
{
    private static final long serialVersionUID = 6826446949682313116L;

    public MyPage()
    {
        Form<Void> form = new Form<Void>("buttonForm");
        add(form);
        form.setOutputMarkupId(true);


        OwsDetailsDialog owsDialog = new OwsDetailsDialog("owsDialog"
                , new CompoundPropertyModel<Ows>(new Ows()))
        {
            @Override
            protected void createOws(AjaxRequestTarget target,
                    IModel<Ows> model)
            {
                Ows ows = model.getObject();
                System.out.println("object: "+ ows);
                close(target);
            }

            @Override
            protected void cancel(AjaxRequestTarget target)
            {
                System.out.println("click auf abbrechen");
                close(target);
            }
        };
        add(owsDialog);

        BootstrapAjaxButton createOwsButton = new BootstrapAjaxButton("createOwsButton",
                Model.of("OWS neu"), Buttons.Type.Default)
        {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form)
            {
                super.onSubmit(target, form);
                owsDialog.setDefaultModelObject(new Ows());
                target.add(owsDialog);
                owsDialog.show(target);
            }
        };

        form.add(createOwsButton);
    }
}
