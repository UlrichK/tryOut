package example.tryOut.ows;

import java.time.LocalDate;

public class OwsWithLocalDate
{
    private Integer index=0;
    private String title = "";
    private String provider = "";
    private String url = "";
    private OwsType category;
    private LocalDate firstPublished =null;
    private Double price =0.0;

    public Double getPrice()
    {
        return price;
    }

    public void setPrice(Double price)
    {
        this.price = price;
    }

    public Integer getIndex()
    {
        return index;
    }

    public void setIndex(Integer index)
    {
        this.index = index;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getProvider()
    {
        return provider;
    }

    public void setProvider(String provider)
    {
        this.provider = provider;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public OwsType getCategory()
    {
        return category;
    }

    public void setCategory(OwsType category)
    {
        this.category = category;
    }

    public LocalDate getFirstPublished()
    {
        return firstPublished;
    }

    public void setFirstPublished(LocalDate firstPublished)
    {
        this.firstPublished = firstPublished;
    }

    @Override
    public String toString()
    {
        return "OwsWithLocalDate{" +
                "index=" + index +
                ", title='" + title + '\'' +
                ", provider='" + provider + '\'' +
                ", url='" + url + '\'' +
                ", category=" + category +
                ", firstPublished=" + firstPublished +
                ", price=" + price +
                '}';
    }
}
