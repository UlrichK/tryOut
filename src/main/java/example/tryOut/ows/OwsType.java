package example.tryOut.ows;

public enum OwsType
{
    WMS(1, "Web Map Service"),
    WFS(2, "Web Feature Service"),
    WMTS(3, "Web Map Tile Service"),
    WCS(4, "Web Coverage Service");

    private final int index;
    private final String name;

    OwsType(int index, String name)
    {
        this.index = index;
        this.name = name;
    }

    public int getIndex()
    {
        return index;
    }

    public String getName()
    {
        return name;
    }
}
