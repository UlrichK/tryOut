package example.tryOut;

import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;

import de.agilecoders.wicket.core.Bootstrap;
import de.agilecoders.wicket.core.settings.BootstrapSettings;
import example.tryOut.ui.MyPage;

public class TryOutApp extends WebApplication
{
    public Class<? extends Page> getHomePage()
    {
        return MyPage.class;
    }

    @Override
    protected void init()
    {
        super.init();

        //this.getComponentInstantiationListeners().add(new SpringComponentInjector(this));
        getMarkupSettings().setDefaultMarkupEncoding("UTF-8");

        BootstrapSettings settings = new BootstrapSettings();
        Bootstrap.install(this, settings);
    }
}
