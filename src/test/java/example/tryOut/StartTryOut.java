package example.tryOut;

import org.eclipse.jetty.server.*;
import org.eclipse.jetty.webapp.WebAppContext;

public class StartTryOut
{

	public static void main(String[] args) {
		System.setProperty("wicket.configuration", "development");

		Server server = new Server();

		HttpConfiguration httpConfig = new HttpConfiguration();
		httpConfig.setSecureScheme("https");
		httpConfig.setSecurePort(8443);
		httpConfig.setOutputBufferSize(32768);

		ServerConnector serverConnector = new ServerConnector(server, new HttpConnectionFactory(httpConfig));
		serverConnector.setPort(8080);
		serverConnector.setIdleTimeout(1000 * 60 * 60);

		server.addConnector(serverConnector);

		WebAppContext bb = new WebAppContext();
		bb.setServer(server);
		bb.setContextPath("/");
		bb.setWar("src/main/webapp");
		server.setHandler(bb);

		try {
			server.start();
			server.join();
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(100);
		}
	}
}
