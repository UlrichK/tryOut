package example.tryOut;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Before;
import org.junit.Test;

import example.tryOut.ui.MyPage;

public class MyPageTest
{
    private WicketTester tester;

    @Before
    public void setUp()
    {
        tester = new WicketTester(new TryOutApp());
    }

    @Test
    public void startPageRendersSuccessfully()
    {
        tester.startPage(MyPage.class);
        tester.assertRenderedPage(MyPage.class);
    }

}
